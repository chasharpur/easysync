const fs = require('fs');
const path = require('path');
const os = require('os');
const tmp = require('tmp');
const spawn = require('await-spawn');
const {gzip, ungzip} = require('node-gzip');
var sites = [];
var command = "";
var default_timestamp = false;
var config;
var num_files = 0;
var done_files = 0;
var failed_files = 0;
var src_dir = "";
var dst_dir = "";
var force = "";
help = "usage: easysync [option] | [-c <command>] [--default-timestamp] [-f up|down] [-s <source>] [-d <destination>]\n"
+"-c: one of list_files - writes a JSON file list to src or destination dir\n"
+"           oneway - performs a one-way sync from source to destination\n"
+"           twoway - performs a two-way sync between source and destination\n"
+"           diffs - precompute diffs between source and destination\n"
+"           delete - delete a file pointed to by -d or -s\n"
+"           timestamps - save timestamps in src and/or dst directories\n"
+"-s <dir>: the absolute source directory path or user@host:path for remote directories\n"
+"-d <dir>: the destination directory with the same syntax as -s\n"
+"-f up|down: resolve conflicts. up=upload down=download. Only used with oneway\n"
+"--default-timestamp: qualifies timestamps command\n"
+"option:\n"
+"--help: print this message\n"
+"--version: print program version"
/**
 * Focus all errors here at a single point.
 * @param log an array of strings
 * @param message the string message to add to log
 */
function log_add(log,message){
    if ( message.indexOf("Cannot read property")!=-1 )
        console.log("error");
    log.push(message);
}
/**
 * Escape a path replacing " and '
 * @param res_path the raw path not escaped
 * @param double true if double-backslashes are to be used
 * @return the path escaped or unchanged if already escaped 
 */
function path_escape(res_path,double) {
    escape_path = "";
    let seen_backslash = false;
    for ( let c of res_path ){
        if ( c == "'" ){
            if ( !seen_backslash ) {
                if ( double )
                    escape_path += "\\'";
                else
                    escape_path += "\'";
            }
            else
                escape_path += c;
        }
        else if ( c == '"' ){
            if ( !seen_backslash ) {
                if ( double )
                    escape_path += '\\"';
                else
                    escape_path += '\"';
            }
            else
                escape_path += c;
        }
        else if ( c == ' ' ) {
            if ( !seen_backslash ) {
                if ( double )
                    escape_path += "\\ ";
                else
                    escape_path += "\ ";
            }
            else
                escape_path += c;
        }
        else 
            escape_path += c;
        seen_backslash = c=='\\'&&!seen_backslash;        
    }
    return escape_path;
}
/**
 * Suround a path or string with double quotes
 * @param res_path the path to quote
 * @return the quoted path
 */
function quote(res_path){
    return '"'+res_path+'"';
}
/**
 * Copy a file from a remote server
 * @param from the path of the file on the remote server
 * @param dst the local destination directory
 * @param desc the file descriptor
 * @param log record errors here
 * @return true if it was copied else false
 */
async function copy_from_remote(from,desc,dst,log) {
    try {
        dst_path = path.join(dst,desc.file_path);
        let args = [];
        let cmd = "scp";
        if ( process.platform=="win32" )
            cmd = "pscp";
        args.push("-p");
        args.push(path_escape(from,true));
        args.push(path_escape(dst_path,false));
        // NB test if local file exists, back up first
        if ( fs.existsSync(dst_path) )
            backup_file(dst_path);
        //console.log(args[1]);
        //console.log(args[2]);
        let bl = await spawn(cmd,args);
        if ( bl.length > 0 )
            log_add(log,bl.toString());
        return bl.length==0;
    } 
    catch(e) {
        if ( e.hasOwnProperty("stderr") )
            log_add(log,e.message+": "+e.stderr.toString());
        return false;
    }
}
/**
 * Set the modification date of the file just saved
 * @param desc object with local/remote path, mod_date and type of destination
 * @param dst the destination directory possibly remote
 * @param platform linux, darwin or win32 of the destination
 * @param tz_offset minutes of timezone offset on remote server
 * @param log save error messages here
 * @return true if it worked
 */
async function set_mod_date(desc,dst,platform,tz_offset,log){
    let res = true;
    try {
        if (!is_local(dst) ){
            let address = parse_address(dst);
            if ( platform == "linux" || platform == "darwin" ){
                dst_path = path.join(address.path,desc.file_path);
                let args = [];
                args.push("-t");
                args.push(to_local_date(desc.mod_date,tz_offset));
                if ( desc.type==2 )
                    args.push("-h");
                args.push(path_escape(dst_path,true));
                res = await exec_remote("touch",address,args,log);
            }
            else {// we can't do win32 remotely because there is 
            // no default tool available in DOS to set mod date
                res = false;
            }
        }
        else {
            let dst_path = path.join(dst,desc.file_path);
            if ( desc.type == 1 ) {
                let date1 = get_date(to_local_date(desc.mod_date));
                try {
                    fs.utimesSync(dst_path,date1,date1);
                }
                catch ( err ){
                    log_add(log,err.message);
                    res = false;
                }
            }
            else if ( platform == "linux" || platform == "darwin" ) {
                const bl = await spawn("touch",["-h","-t",to_local_date(desc.mod_date),dst_path]);
                if ( bl.length > 0 )
                    log_add(log,bl.toString());
            }
            else {
                // local windows, symbolic link
                let win_dst_path = path.join(dst,".link",desc.file_path);
                if ( !fs.existsSync(win_dst_path) ) {
                    let win_parent = path.dirname(win_dst_path);
                    fs.mkdirSync(win_parent,{recursive:true});
                }
                let date1 = get_date(to_local_date(desc.mod_date));
                try {
                    let fd = fs.openSync(win_dst_path, 'w');
                    fs.utimesSync(win_dst_path,date1,date1);
                    fs.closeSync(fd);
                }
                catch ( err ){
                    log_add(log,err.message);
                    res = false;
                }
            }
        }
    }
    catch ( err ) {
        if ( err.hasOwnProperty("stderr") )
            log_add(log,e.message+": "+e.stderr.toString());
        res = false;
    }
    return res;
}
/**
 * Copy a local file to a remote address
 * @param from full path to source 
 * @param desc specification of file 
 * @param dst the destnation directory possibly remote
 * @param log array of string to add to if it goes wrong
 * @return true if it worked
 */
async function copy_to_remote(from,desc,dst,log) {
    let res = true;
    try {
        dst_path = dst;
        dst_path = path.join(dst_path,desc.file_path);
        let args = [];
        args.push("-p");
        args.push(path_escape(from,false));
        args.push(path_escape(dst_path,true));
        //console.log(args[1]);
        //console.log(args[2]);
        let cmd = "scp";
        if ( process.platform=="win32" )
            cmd = "pscp";
        let bl = await spawn(cmd,args);
        if ( bl.length > 0 )
            log_add(log,bl.toString());
        return bl.length==0;
    } 
    catch(e) {
        if ( e.hasOwnProperty("stderr") )
            log_add(log,e.message+": "+e.stderr.toString());
        res = false;
    }
    return res;
}
/**
 * Execute a command on a remote machine
 * @param cmd the command to execute
 * @param address the parsed remote address
 * @param args the commands arguments
 * @param log record errors here
 * @return true if it worked
 */
async function exec_remote(cmd,address,args,log){
    let res = true;
    try {
        args.unshift(cmd);
        let host = address.host;
        if ( address.hasOwnProperty('username') && address.username.length>0 ){
            if ( address.hasOwnProperty("password") && address.password.length>0 )
                host = address.username+':'+address.password+'@'+address.host;
            else
                host = address.username+'@'+address.host;
        }
        args.unshift(host);
        let bl = await spawn('ssh',args);
        if ( bl.length > 0 ) {
            res = false;
            log_add(log,bl.toString());
        }
    }
    catch ( err ){
        let message = (err.stderr.length>0)?err.stderr.toString():err.message;
        log_add(log,message);
        res = false;
    }
    return res;
}
/**
 * Is a directory specification local (bare) or remote (scp format)
 * @param dir the directory path
 */
function is_local(dir) {
    if ( dir.indexOf(":") == -1 )
        return true;
    else if ( dir.match(/[A-Z]:\\/) ) {
        return true;
    }
    else
        return false;
}
function pad(num, size) {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
}
/**
 * Convert a date object to a string compatible with touch -t (Mac/Linux)
 * @param d a utc date object
 */
function get_time_string_utc(d){
    let time = d.getUTCFullYear().toString();
    time += pad(d.getUTCMonth()+1,2);
    time += pad(d.getUTCDate(),2);
    time += pad(d.getUTCHours(),2);
    time += pad(d.getUTCMinutes(),2);
    time += "."+pad(d.getUTCSeconds(),2);
    return time;
}
/**
 * Get a date object from a UTC time stamp
 * @param time the time string in UTC time, format: YYYYMMDDHHMM.SS
 * @return a date object specified using UTC as UTC time
 */
function get_date_utc(time){
    let year = parseInt(time.substring(0,4));
    let month = parseInt(time.substring(4,6))-1;
    let day = parseInt(time.substring(6,8));
    let hour = parseInt(time.substring(8,10));
    let mins = parseInt(time.substring(10,12));
    let secs = parseInt(time.substring(13,15));
    let utc_millis = Date.UTC(year, month, day, hour, mins, secs);
    return new Date(utc_millis);
}
/**
 * Convert a utc time string to its local value
 * @param date_str_utc the utc time string (yyyymmddhhmm.ss)
 * @param tz_offset optional timezeone offset in minutes from local to utc
 */
function to_local_date(date_str_utc,tz_offset){
    let date_utc = get_date_utc(date_str_utc);
    if ( tz_offset==null )
        tz_offset = date_utc.getTimezoneOffset();
    let new_millis = date_utc.getTime()-(tz_offset*60000);
    let new_date = new Date(new_millis);
    return get_time_string_utc(new_date);
}
/**
 * List files locally. For remote listing run this on the remote server.
 * @param parent_dir the parent directory of all files to be listed
 * @param rel_path the relative path from parent dir to the file
 * @param list an array of full paths of files listed. updated on exit.
 */
function list_files(parent_dir,rel_path,list,log){
    let dir_path = path.join(parent_dir,rel_path);
    let files = fs.readdirSync(dir_path);
    for ( let file of files ) {
        if ( file != ".sync") {
            let res_path = path.join(dir_path,file);
            if ( fs.existsSync(res_path) ) {
                let stat = fs.lstatSync(res_path);
                if ( stat.isDirectory() ){
                    let rel_dir = path.join(rel_path,file);
                    list_files(parent_dir,rel_dir,list,log);
                }
                else if ( stat.isSymbolicLink() ){
                    if ( process.platform == "win32" ) {
                        let win_file_path = path.join(parent_dir,".link",rel_path,file);
                        if ( fs.existsSync(win_file_path) )
                            stat = fs.lstatSync(win_file_path);
                    }
                    let file_path = path.join(rel_path,file);
                    let target = fs.readlinkSync(res_path);
                    // NB this is UTC time
                    let date = stat.mtime;
                    list.push({file_path:file_path,mod_date:get_time_string_utc(date),type:2,target:target});
                }
                else if ( stat.isFile() ) {
                    let file_path = path.join(rel_path,file);
                    // NB this is UTC time
                    let date = stat.mtime;
                    list.push({file_path:file_path,mod_date:get_time_string_utc(date),type:1,size:stat.size});
                }
            }
            else
                log_add(log,res_path+" not found");
        }
    }
}
/**
 * convert a list of file descriptions to an object
 * @param list a complete file list from src or dst
 * @return an object relative path->file description (minus the file_path)
 */
function list_to_obj(list) {
    let obj = {};
    for ( let item of list ){
        let p = item.file_path;
        if ( p == '07:35:07')
            console.log('07:35:07');
        delete item.file_path;
        obj[p] = item;
    }
    return obj;
}
/**
 * Read the user's ssh config file in home directory
 * @param log the log string array to record errors in
 */
function read_config(log){
    if ( config == undefined ){
        config = {};
        let config_path = path.join(os.homedir(),".easysync","config.json");
        if ( fs.existsSync(config_path) ) {
            let config = JSON.parse(fs.readFileSync(config_path,{encoding:"utf8"}));
            if ( config.hasOwnProperty("sites") ){
                sites = config.sites;
            }
        }
        else
            log_add(log,"no config found ("+config_path+")");
    }
}
/**
 * Copy a file from source to destination
 * @param src full source file path
 * @param dst_desc the file descriptor
 * @param dst the destination directory possibly remote
 * @param log log errors here
 * @return true if the file was copied
 */
async function copy_file(src,desc,dst,log){
    let res = true;
    if ( !is_local(src) ) {
        res = await copy_from_remote(src,desc,dst,log);
    }
    else if ( fs.existsSync(src) ){
        let parent = path.dirname(src);
        if ( !fs.existsSync(parent) )
            fs.mkdir(parent,{recursive:true});
        let dst_path = path.join(dst,desc.file_path);
        if ( fs.existsSync(dst_path) )
            backup_file(dst_path);
        fs.copyFileSync(src,dst_path);
        // file saved is always local
        res = await set_mod_date(desc,dst,process.platform,null,log)
    }
    else {
        log_add(log,"missing src or dst "+src);
        res = false;
    }
    return res;
}
/**
 * Count the number of files in a set of diffs (for progress bar)
 * @param set a set (source or dest diffs)
 * @return the number of files in the set
 */
function count_files(set) {
    let num_files = set.updates.length+set.new.length+set.deletes.length;
    return num_files;
}
/**
 * Ensure that a directory exists on the remote site
 * @param dst the remote destination dir
 * @param dir the relative directory in dst to create or confirm
 * @param log add errors to this list
 * @return true if it was created or already existed
 */
async function ensure_remote_dir(dst,dir,log) {
    let esc_dir = path_escape(dir,true);
    let args = [];
    let address = parse_address(dst);
    let res_path = path.join(address.path,esc_dir);
    args.push("-p");
    args.push(res_path);
    return await exec_remote("mkdir",address,args,log);
}
function remove_empty_parents(res_path){
   let parent = path.dirname(res_path);
    let p_files = fs.readdirSync(parent);
    while ( p_files.length==0 ) {
        fs.rmdirSync(parent);
        parent = path.dirname(parent);
        p_files = fs.readdirSync(parent);
    }
}
/**
 * Carry out the computed diffs for real
 * @param set the set of diffs for src or dst
 * @param src the path of the source
 * @param dst the path of the destination
 * @param stamp the destination time-stamp object
 * @param log an array of strings
 * @return true if something was changed
 */
async function execute_diffs(set,src,dst,stamp,log){
    let res = true;
    if ( "deletes" in set ) {
        for ( let del of set.deletes ){
            res = true;
            if ( is_local(dst) ) {
                let res_path = path.join(dst,del.file_path);
                try {
                    if ( fs.existsSync(res_path) ){
                        backup_file(res_path);
                        log_add(log,"deleting "+res_path);
                        fs.unlinkSync(res_path);
                        remove_empty_parents(res_path);
                    }
                    else{
                        try { 
                            // it's either a missing file or a broken link
                            // throws an exception if it is not there
                            let target = fs.readlinkSync(res_path);
                            // link file exists but not target
                            let target_path = path.join(path.dirname(res_path),target);
                            if ( !fs.existsSync(target_path) ){
                                // delete broken link
                                log_add(log,"deleting broken link "+res_path);
                                fs.unlinkSync(res_path);
                                remove_empty_parents(res_path);
                            }
                        }
                        catch ( err ) {
                            // file to delete is simply not there
                            log_add(log,"file to delete is not present: "+res_path);
                            res = false;
                            break;
                        }
                    }
                }
                catch ( err ) {
                    log_add(log,err.message);
                    res = false;
                    break;
                }
            }
            else {
                let address = parse_address(dst);
                let res_path = path.join(address.path,del.file_path);
                // delete command removes empty parent directores
                let args = [];
                args.push("-c");
                args.push("delete");
                args.push("-d");
                args.push(path_escape(res_path,true));
                log_add(log,"deleting remote file "+res_path);
                res = await exec_remote("easysync",address,args,log);
                if ( !res )
                    break;
            }
            update_progress(res);
        }
    }
    if ( "updates" in set ) {
        for ( let update of set.updates ) {
            if ( is_local(dst) ) {
                //console.log("copying local file "+update.file_path);
                res = await save_local_file(src,dst,update,log);
            }
            else {
                //console.log("saving remote file "+update.file_path);
                res = await save_remote_file(src,dst,update,stamp,log);
            }
        }
    }
    if ( "new" in set ) { 
        // ensure new remote files have parent directories
        if ( !is_local(dst) ) {
            let dirs = {};
            for ( let new_file of set.new ) {
                let dir = path.dirname(new_file.file_path)
                dirs[dir] = true;
            }
            for ( let dir in dirs ) {
                res = await ensure_remote_dir(dst,dir,log);
                if ( !res )
                    return res;
            }
        }
        for ( let new_file of set.new ) {
            if ( is_local(dst) ) {
                //console.log("downloading new file "+new_file.file_path);
                res = await save_local_file(src,dst,new_file,log);
            }
            else {
                //console.log("copying new file to remote "+new_file.file_path);
                res = await save_remote_file(src,dst,new_file,stamp,log);
            }
        }
    }
    return res;
}
function update_progress(succeeded){
    if ( num_files > 0 ) {
        let old = Math.floor(done_files*100/num_files)
        done_files++;
        if ( done_files == num_files )
            console.log("100%");
        else {
            let percent = Math.floor(done_files*100/num_files);
            if ( percent > old )
                console.log(percent+"%");
        }
        if ( !succeeded )
            failed_files++;
    }
}
/**
 * Save a file on the local file system
 * @param src the source directory (possibly remote)
 * @param dst the local destination directory
 * @param desc the file descriptor from the file list
 * @param log an array of strings to record errors in
 * @return true if it worked
 */
async function save_local_file(src,dst,desc,log) {
    let res = true;
    let dst_path = path.join(dst,desc.file_path);
    try {
        if ( desc.type==1 ) {
            let src_path = path.join(src,desc.file_path);
            let parent = path.dirname(dst_path);
            if ( !fs.existsSync(parent) ) 
                fs.mkdirSync(parent,{recursive:true});
            res = await copy_file(src_path,desc,dst,log);
        }
        else {
            let parent = path.dirname(dst_path);
            if ( !fs.existsSync(parent) )
                fs.mkdirSync(parent,{recursive:true});
            if ( fs.existsSync(dst_path) ){
                // don't backup links
                //backup_file(dst_path);
                fs.unlinkSync(dst_path);
            }
            fs.symlinkSync(desc.target,dst_path,"file");
            /*if ( dst_path.indexOf(".") != -1 )
                console.log("creating local symlink "+dst_path);*/
            res = await set_mod_date(desc,dst,process.platform,null,log);
        }
    }
    catch ( err ) {
        if ( err.hasOwnProperty("stderr") )
            log_add(log,err.stderr.toString());
        res = false;
    }
    update_progress(res);
    return res;
}
/**
 * Test if a remote file exists
 * @param dst_address the parsed dst_dir
 * @param res_path the user-relative path to the resource
 * @param stamp the timestamp of the remote dir
 * @param log record errors here
 */
async function remote_file_exists(dst_address,res_path,stamp,log ) {
    let args = [];
    if ( stamp.platform != "win32" ) {
        args.push(res_path);
        args.push("|");
        args.push("wc");
        args.push("-w");
        res = await exec_remote('ls',dst_address,args,log);
        if ( !res ) {
            let num = log.pop().trim();
            return num == "1";
        }
    }
    else
        return false;// implement later
}
/**
 * Save a file on a remote filesystem
 * @param src directory must be local
 * @param dst remote directory or specification
 * @param desc the file descriptor from file list
 * @param stamp the destination time stamp
 * @param log the log message array
 * @return true if it worked
 */
async function save_remote_file(src,dst,desc,stamp,log){
    let res = true;
    let dst_address = parse_address(dst);
    let dst_path = path.join(dst_address.path,desc.file_path);
    if ( desc.type==2 ) {//symlink
        let parent = path.dirname(dst_path);
        if ( stamp.platform == "win32" ) {
            // have to delete symlink first
            let del_args = [];
            del_args.push(dst_path);
            res = await exec_remote('del',dst_adddress,del_args,log);
            if ( res ){
                let link_args = [];
                link_args.push(dst_path);
                link_args.push(desc.target);
                res = await exec_remote('mklink',dst_address,link_args,log);
            }
            if ( res )
                res = await set_mod_date(desc,dst,stamp.platform,stamp.tz_offset,log);
        }
        else {
            if ( process.platform == "win32" ) {
                dst_path = dst_path.replace(/\\/g,"/");
                parent = parent.replace(/\\/g,"/");
            }
            let rm_args = [];
            rm_args.push("-f");
            rm_args.push(path_escape(dst_path,true));
            res = await exec_remote('rm',dst_address,rm_args,log);
            if ( res && await remote_file_exists(dst_address,rm_args.pop(),stamp,log) )
                res = false;
            if ( res ) {
                let mkdir_args = [];
                mkdir_args.push("-p");
                mkdir_args.push(path_escape(parent,true));
                res = await exec_remote('mkdir',dst_address,mkdir_args,log);
            }
            if ( res ){
                let ln_args = [];
                ln_args.push("-s");
                ln_args.push(path_escape(desc.target,true));
                ln_args.push(path_escape(dst_path,true));
                /*if ( dst_path.indexOf(".") != -1 )
                    console.log("creating link name "+dst_path);*/
                res = await exec_remote('ln',dst_address,ln_args,log);
            }
            if ( res )
                res = await set_mod_date(desc,dst,stamp.platform,stamp.tz_offset,log);
        }
    }
    else {
        let src_path = path.join(src,desc.file_path);
        res = await copy_to_remote(src_path,desc,dst,log);
    }
    update_progress(res);
    return res;
}
/**
 * Resolve conflicting files by overwriting them
 * @param conflicts an array of file descriptors from diffs
 * @param src the source directory
 * @param dst the destination directory
 * @param stamp the timestamp object for dst
 * @param log array of messages
 */
async function resolve_conflicts( conflicts, src, dst, stamp, log ) {
    let res = true;
    if ( force == "up" ) {
        for ( let conflict of conflicts ){
            if ( is_local(dst) ) 
                res = await save_local_file(src,dst,conflict,log);
            else
                res = await save_remote_file(src,dst,conflict,stamp,log);
            update_progress(res);
        }
    }
    else if ( force == "down" ){
        for ( let conflict of conflicts ){
            if ( is_local(src) ) 
                res = await save_local_file(dst,src,conflict,log);
            else
                res = await save_remote_file(dst,src,conflict,stamp,log);
            update_progress(res);
        }
    }
}
/**
 * Write the current date/time to the given dir
 * @param dir the dir to write to
 * @param log add any errors here
 * @return true if it worked
 */
async function save_time_stamp(dir,log){
    let res = true;
    let obj = {};
    let date = (default_timestamp)?new Date("1 January 2001"):new Date();
    obj.time_stamp = date.toUTCString();
    obj.user = require("os").userInfo().username;
    obj.platform = process.platform;
    obj.tz_offset = date.getTimezoneOffset();
    let stamp_path = path.join(dir,".sync","stamp.json");
    if ( is_local(dir) ) {
        try {
            let parent = path.dirname(stamp_path);
            if ( !fs.existsSync(parent) )
                fs.mkdirSync(parent,{recursive:true});
            if ( fs.existsSync(stamp_path) )
                fs.unlinkSync(stamp_path);
            fs.writeFileSync(stamp_path,JSON.stringify(obj));
        }
        catch ( err ) {
            log_add(log,err.message);
            res = false;
        }
    }
    else { //generate remote timestamp
        let address = parse_address(dir);
        let args = [];
        args.push("-c");
        args.push("timestamps");
        if ( default_timestamp )
            args.push("--default-timestamp");
        args.push("-d")
        args.push(address.path);
        res = await exec_remote("easysync",address,args,log);
    }
    return res;
}
/**
 * Read a time stamp in the given directory
 * @param dir the directory to read from
 * @return an object with time_stamp and user fields or a default timestamp
 */
async function read_time_stamp(dir,log){
    if ( is_local(dir) ) {
        let stamp_path = path.join(dir,".sync","stamp.json");
        if ( fs.existsSync(stamp_path) ){
            let obj = JSON.parse(fs.readFileSync(stamp_path,{encoding:"utf8"}));
            if ( !obj.hasOwnProperty("platform") )
                obj.platform = process.platform;
            if ( !obj.hasOwnProperty("username") )
                obj.username = os.userInfo().username;
            if ( !obj.hasOwnProperty("tz_offset") )
                obj.tz_offset = new Date().getTimezoneOffset();
            return {time_stamp:new Date(obj.time_stamp),user:obj.user,platform:obj.platform,tz_offset:obj.tz_offset};
        }
        else {
            let time = new Date("1 January 2001");
            return {time_stamp:time,username:"anonymous",platform:process.platform,tz_offset:new Date().getTimezoneOffset()};
        }
    }
    else {
        let res_path = path.join(dir,".sync","stamp.json");
        const tmpobj = tmp.fileSync();
        let tmp_name = path.basename(tmpobj.name);
        let tmp_dir = path.dirname(tmpobj.name);
        let desc = {file_path:tmp_name,type:1,mod_date:new Date().toUTCString()};
        let res = await copy_from_remote(res_path,desc,tmp_dir,log);
        if ( !res ) {
            // no current timestamp file - generate default and download it
            let address = parse_address(dir);
            let args = [];
            args.push("-c");
            args.push("timestamps");
            args.push("--default-timestamp");
            args.push("-d");
            args.push(address.path);
            res = await exec_remote("easysync",address,args,log);
            if ( res )
                res = await copy_from_remote(res_path,desc,tmp_dir,log);
        }
        if ( res ) {
            try {
                let timestamp = fs.readFileSync(tmpobj.name,{encoding:"utf8"});
                tmpobj.removeCallback();
                let obj = JSON.parse(timestamp);
                obj.time_stamp = new Date(obj.time_stamp);
                return obj;
            }
            catch ( err ) {
                log_add(log,err.message);
            }
        }
        // return something
        return {time_stamp:new Date(),user:os.userInfo().username,platform:process.platform};
    }
}
/**
 * Parse a remote address in scp-style
 * @param address the string [user[:password]]@host:path
 */
function parse_address(address) {
    let spec = {};
    let last_colon = address.lastIndexOf(":");
    spec.path = address.substring(last_colon+1);
    let rest = address.substring(0,last_colon);
    let at_pos = rest.indexOf("@");
    if ( at_pos != -1 ){
        spec.host = rest.substring(at_pos+1);
        rest = rest.substring(0,at_pos);
        let colon_pos = rest.indexOf(":");
        if ( colon_pos == -1 )
            spec.username = rest;
        else {
            spec.username = rest.substring(0,colon_pos);
            spec.password = rest.substring(colon_pos+1);
        }
    }
    else
        spec.host = rest;
    return spec;
}
function is_different(dsize,ssize,dst_mod,src_mod,dst_link,src_link) {
    // if the size differs, they are different
    if ( dsize != undefined && ssize != undefined && dsize != ssize )
        return true;
    // else maybe it is a link, then compare targets, mod_dates don't matter
    else if ( dst_link != undefined && src_link != undefined ) 
        return dst_link != src_link;
    // if the change is older than 1 hour consider it different
    else if ( Math.abs(dst_mod-src_mod) > 3600000 )
        return true;
    // link and/or size the same, modification recent
    else
        return false;
}
/**
 * Compare two lists bidirectionally
 * @param src_list file list generated from src_dir
 * @param dst_list file list generated from dst_dir
 * @param src_stamp time-stamp of last sync on src_dir
 * @param dst_stamp time stamp of last sync on dst_dir
 * @param log record errors here
 * @return diffs deletes, new, updates, conflicts for src and dst
 */
function compare_lists(src_list,dst_list,src_stamp,dst_stamp,log){
    let src = list_to_obj(src_list);
    let dst = list_to_obj(dst_list);
    let diffs = {src:{new:[],updates:[],deletes:[]},
        dst:{new:[],updates:[],deletes:[]},conflicts:[]};
    // NB UTC time
    let src_time = (src_stamp==undefined)?undefined:src_stamp.time_stamp.getTime();
    let dst_time = (dst_stamp==undefined)?undefined:dst_stamp.time_stamp.getTime();
    for ( let key in src ){
        if ( path.basename(key) == '.DS_Store' )
            continue;
        if ( key in dst ) {
            let dsrc = get_date_utc(src[key].mod_date);
            let ddst = get_date_utc(dst[key].mod_date);
            // NB UTC time in milliseconds
            let src_mod = dsrc.getTime();
            let dst_mod = ddst.getTime();
            // for links
            let dst_link = (dst[key].hasOwnProperty("target"))?dst[key].target:undefined;
            let src_link = (src[key].hasOwnProperty("target"))?src[key].target:undefined;
            // file size
            let dsize = (dst[key].hasOwnProperty('size'))?dst[key].size:undefined;
            let ssize = (src[key].hasOwnProperty('size'))?src[key].size:undefined;
            // check if the types differ
            let types_differ = dst[key].type != src[key].type;
            if ( types_differ ){
                log_add(log,"cannot replace symlink by a file ("+key+")");
                continue;
            }
            // hold difference in variable
            let different = is_different(dsize,ssize,dst_mod,src_mod,dst_link,src_link);
            // this section is solid
            if ( src_time == undefined || dst_time == undefined || src_time==dst_time ) {
                if ( dst_mod > src_mod ) {
                    // updates contain files that need updating from the other side TO them
                    // src.updates are files in src_dir that need replacing by the version in dst_dir
                    dst[key].file_path = key;
                    if ( different )
                        diffs.src.updates.push(dst[key]);
                    //console.log("src will be updated: file="+key+" from mod_date:"+src[key].mod_date+" to "+dst[key].mod_date);
                }
                else if ( dst_mod < src_mod ){
                    src[key].file_path = key;
                    if ( different )
                        diffs.dst.updates.push(src[key]);
                    //console.log("dst will be updated: file="+key+" from mod_date:"+dst[key].mod_date+" to "+src[key].mod_date);
                }
            }
            // this is solid
            else if ( dst_time < src_time ) {
                // we have priority
                if ( dst_mod < src_mod ) {
                    src[key].file_path = key;
                    if ( different )
                        diffs.dst.updates.push(src[key]);
                    //console.log("dst will be updated: file="+key+" from mod_date:"+dst[key].mod_date+" to "+src[key].mod_date);
                }
                else if ( dst_mod > src_mod ) {
                    dst[key].file_path = key;
                    if ( different )
                        diffs.src.updates.push(dst[key]);
                    //console.log("src will be updated: file="+key+" from mod_date:"+src[key].mod_date+" to "+dst[key].mod_date);
                }
            }
            else {
                // dst_stamp > src_stamp: dst modified after we synced last
                // this looks correct too
                if ( dst_mod > src_mod ) {
                    // modified locally AND remotely
                    if ( src_mod > src_time ) {
                        src[key].file_path = key;
                        diffs.conflicts.push(src[key]);
                    }
                    else {
                        dst[key].file_path = key;
                        if ( different )
                            diffs.src.updates.push(dst[key]);
                        //console.log("src will be updated: file="+key+" from mod_date:"+src[key].mod_date+" to "+dst[key].mod_date);
                    }
                }
                else if ( dst_mod < src_mod ){
                    if ( dst_mod > src_time ) {
                        src[key].file_path = key;
                        diffs.conflicts.push(src[key]);
                    }
                    else {
                        src[key].file_path = key;
                        if ( different )
                            diffs.dst.updates.push(src[key]);
                        //console.log("dst will be updated: file="+key+" from mod_date:"+src[key].mod_date+" to "+dst[key].mod_date);
                    }
                }
            }
        }
        else if ( dst_time != undefined ) {
            // src has it but dst doesn't
            src[key].file_path = key;
            let file_mod_date = get_date_utc(src[key].mod_date).getTime();
            if ( file_mod_date > src_time || file_mod_date > dst_time ) 
                diffs.dst.new.push(src[key]);
            else
                diffs.src.deletes.push(src[key]);
        }
    }
    if ( src_time != undefined ) {
        for ( let key in dst ) {
            if ( ! (key in src) ){
                // dst has it but src doesn't
                dst[key].file_path = key;
                let file_mod_date = get_date_utc(dst[key].mod_date).getTime();
                if ( file_mod_date > dst_time || file_mod_date > src_time )
                    diffs.src.new.push(dst[key]);
                else
                    diffs.dst.deletes.push(dst[key]);
            }
        }
    }
    return diffs;
}
/**
 * Get or create a local file list
 * @param dir the directory it is in
 * @param file_list an empty array
 * @param log log errors here
 * @return an array of file descriptions: (relative) file_path, mod_date, type and optionally target (if type 2)
 */
async function fetch_file_list(dir,file_list,log){
    let res = true;
    let file_list_path = path.join(dir,".sync","file_list.json.gz");
    if ( fs.existsSync(file_list_path) ) {
        let file_list_json = await ungzip(fs.readFileSync(file_list_path));
        file_list = JSON.parse(file_list_json);
    }
    else {
        res = await build_file_list(src_dir,file_list,log);
    }
    return res;
}
/**
 * Compute diffs between src and dst and store in <src_dir>/.sync
 * @param log an array of log messages
 */
async function compute_diffs(log){
    let res = await fetch_file_list(src_dir,src_list,log);
    if ( res ) {
        res = await fetch_file_list(dst_dir,dst_list,log);
        if ( res ) {
            let src_stamp = await read_time_stamp(src_dir,log);
            let dst_stamp = await read_time_stamp(dst_dir,log);
            let diffs = compare_lists(src_list,dst_list,src_stamp,dst_stamp,log);
            let parent = path.join(src_dir,".sync");
            if ( !fs.existsSync(parent) )
                fs.mkdirSync(parent,{recursive:true});
            let diffs_path = path.join(parent,"diffs.json");
            fs.writeFileSync(diffs_path,JSON.stringify(diffs));
            if ( diffs.conflicts.length>0 )
                log_add(log,"found conflicts in "+diffs.conflicts.length+" files by "+dst_time_stamp.user);
        }
    }
    return res;
}
/**
 * Build a file_list locally or remotely
 * @param dir the directory specification
 * @param file_list the list of files to create
 * @param log record errors here
 * @return a result true or false
 */
async function build_file_list(dir,file_list,log){
    try {
        let res = true;
        if ( !is_local(dir) ) {
            let address = parse_address(dir);
            let args = [];
            args.push("-s");
            args.push(address.path);
            args.push("-c");
            args.push("list_files");
            res = await exec_remote("easysync",address,args,log);
            if ( res ) {
                const tmpobj = tmp.fileSync();
                let res_path = path.join(dir,".sync","file_list.json.gz");
                let tmp_name = path.basename(tmpobj.name);
                let tmp_dir = path.dirname(tmpobj.name);
                // ignores time in desc - just uses path
                let desc = {file_path:tmp_name,type:1,mod_date:new Date().toUTCString()};
                res = await copy_from_remote(res_path,desc,tmp_dir,log);
                if ( res ) {
                    let gzipped_file_list = fs.readFileSync(tmpobj.name);
                    let json = await ungzip(gzipped_file_list);
                    tmpobj.removeCallback();
                    let j_obj = JSON.parse(json);
                    file_list.push.apply(file_list,j_obj);
                    return true;
                }
            }
            if ( !res )
                throw {name:"ReferenceError",message:"couldn't read remote file list"};
        }
        else {
            list_files(dir,"",file_list,log);
            //var end_time = get_time();
            //console.log("scanned "+file_list.length+" files in "+(end_time-start_time)/1000000000+" secs");
        }
        return res;
    }
    catch ( err ){
        log_add(log,err.message);
        return false;
    }
} 
function print_conflicts(conflicts,src,dst,src_stamp,dst_stamp,log) {
    for ( let conflict of conflicts ){
        if ( !is_local(dst) ){
            let address = parse_address(dst);
            log_add(log,conflict.file_path+" conflicts on "+address.host+" updated by "+dst_stamp.user+" on "+dst_stamp.time_stamp.toString());
        }
        else if ( !is_local(src) ){
            let address = parse_address(src);
            log_add(log,conflict.file_path+" conflicts with "+address.host+" updated by "+src_stamp.user+" on "+src_stamp.time_stamp.toString());
        }
        else
            log_add(log,conflict.file_path+" conflicts with local copy updated by "+dst_stamp.user+" on "+dst_stamp.time_stamp.toString());
    }
}
/**
 * Sync both directories each way
 * @param log an array of log messages
 */
async function two_way_sync(log){
    try {
        console.log("comparing timestamps");
        let s_stamp = await read_time_stamp(src_dir,log);
        let d_stamp = await read_time_stamp(dst_dir,log);
        console.log("fetching file lists (two-way)");
        let src_list = [];
        let dst_list = [];
        let res = await build_file_list(src_dir,src_list,log);
        if ( res )
            res = await build_file_list(dst_dir,dst_list,log);
        if ( res ) {
            let diffs = compare_lists(src_list,dst_list,s_stamp,d_stamp,log);
            let num_dst_files = count_files(diffs.dst);
            let num_src_files = count_files(diffs.src);
            let num_conflicts = (force.length>0)?diffs.conflicts.length:0;
            num_files = num_src_files+num_dst_files+num_conflicts;
            if ( num_files > 0 ) {
                let dst_res = false;
                let suffix = " files";
                let src_res = false;
                done_files = 0;
                console.log("0%");
                if ( num_dst_files > 0 ) {
                    suffix = (num_files==1)?" file":" files";
                    console.log("syncing up "+num_dst_files+suffix);
                    dst_res = await execute_diffs(diffs.dst,src_dir,dst_dir,d_stamp,log);
                }
                if ( num_src_files > 0 ) {
                    suffix = (num_src_files==1)?" file":" files";
                    console.log("syncing down "+num_src_files+suffix);
                    try {
                        src_res = await execute_diffs(diffs.src,dst_dir,src_dir,s_stamp,log);
                        if ( src_res && num_conflicts > 0 ){
                            console.log("resolving "+num_conflicts+" conflicts");
                            if ( force == "up" || force == "down" )
                                await resolve_conflicts(diffs.conflicts,src_dir,dst_dir,d_stamp,log);
                            else
                                print_conflicts(diffs.conflicts,src_dir,dst_dir,s_stamp,d_stamp,log);
                        }
                    }
                    catch ( err ) {
                        log_add(log,err.message);
                    }
                }
                if ( dst_res && num_dst_files > 0 )
                    await save_time_stamp(dst_dir,log);
                if ( src_res && num_src_files > 0 )
                    await save_time_stamp(src_dir,log);
                suffix = (done_files==1)?" file":" files";
                console.log("synced "+done_files+suffix);
            }
            else
                console.log("nothing to do");
        }
    }
    catch ( err ) {
        log_add(log,err.message);
    }
}
/**
 * Sync source to destination directory only
 * @param log record any errors/messages here
 */
async function one_way_sync(log){
    try {
        console.log("comparing timestamps");
        let s_stamp = await read_time_stamp(src_dir,log);
        let d_stamp = await read_time_stamp(dst_dir,log);
        console.log("fetching file lists (one-way)");
        let src_list = [];
        let dst_list = [];
        let res = await build_file_list(src_dir,src_list,log);
        if ( res ) {
            res = await build_file_list(dst_dir,dst_list,log);
            if ( res ) {
                let diffs = compare_lists(src_list,dst_list,s_stamp,d_stamp,log);
                let num_conflicts = diffs.conflicts.length;
                num_files = count_files(diffs.dst)+num_conflicts;
                if ( num_files > 0 ) {
                    let suffix = (num_files==1)?" file":" files";
                    console.log("syncing "+num_files+suffix);
                    done_files = 0;
                    console.log("0%");
                    let res = await execute_diffs(diffs.dst,src_dir,dst_dir,d_stamp,log);
                    if ( num_conflicts > 0 ) {
                        console.log("resolving "+num_conflicts+" conflicts");
                        await resolve_conflicts(diffs.conflicts,src_dir,dst_dir,d_stamp,log);
                    }
                    if ( res ) {
                        await save_time_stamp(src_dir,log);
                        await save_time_stamp(dst_dir,log);
                    }
                    suffix = (done_files==1)?" file":" files";
                    log_add(log,"synced "+done_files+suffix);
                }
                else
                    console.log("nothing to do");
            }
        }
    }
    catch ( err ) {
        log_add(log,err.message);
    }
}
/**
 * Write out the file list locally to .sync/file_list.json.gz
 */
async function write_file_list(log){
    let dir = src_dir;
    if ( dir.length==0 )
        dir = dst_dir;
    if ( dir.length > 0 ) {
        let list = [];
        let res = await build_file_list(dir,list,log);
        let parent = path.join(dir,".sync");
        if ( res ) {
            let list_path = path.join(parent,"file_list.json.gz");
            if ( !fs.existsSync(parent) )
                fs.mkdirSync(parent,{recursive:true});
            let gzipped_json = await gzip(JSON.stringify(list));
            fs.writeFileSync(list_path,gzipped_json);
        }
    }
    else
        log_add(log,"specify source or destination dir");
}
/**
 * Make a backup of a file we are going to delete locally
 * @param file the file to backup
 */
function backup_file(file){
    try {
        let orig_file = file;
        if ( file.indexOf("/")==0 )
            file = "."+file;
        let backup_path = path.join(os.homedir(),".easysync",".backup",file);
        fs.mkdirSync(path.dirname(backup_path),{recursive:true});
        fs.copyFileSync(orig_file,backup_path);
    }
    catch ( err ) {
        console.log("failed to backup file "+file+" because of "+err.message);
    }
}
/**
 * Delete file is a local delete only. 
 * To delete remote files call easysync -c delete -d <file>
 * @param log add error messages here
 */
function delete_file(log){
    let file = src_dir;
    if ( file.length == 0 )
        file = dst_dir;
    if ( is_local(file) != -1 ){
        if ( fs.existsSync(file) ){
            backup_file(file);
            fs.unlinkSync(file);
            let parent = path.dirname(file);
            let p_files = fs.readdirSync(parent);
            while ( p_files.length==0 ) {
                fs.rmdirSync(parent);
                parent = path.dirname(parent);
                p_files = fs.readdirSync(parent);
            }
        }
        else
            log_add(log, "file "+file+" not found");
    }
    else 
        log_add(log,"can't delete remote file "+file);
}
/** 
 * read commandline args
 * @return true if they checked out else false
 */
function read_args() {
    let sane = true;
    for ( let i=0;i<process.argv.length;i++ ) {
        if ( i > 0 ){
            if ( process.argv[i-1]=='-s' )
                src_dir = process.argv[i];
            else if ( process.argv[i-1] == '-f' )
                force = process.argv[i];
            else if ( process.argv[i-1] == '-d' )
                dst_dir = process.argv[i];
            else if ( process.argv[i-1] == '-c' ){
                if ( process.argv[i] == 'list_files')
                    command = "list_files";
                else if ( process.argv[i] == 'oneway' )
                    command = 'oneway';
                else if ( process.argv[i] == 'twoway' )
                    command = 'twoway';
                else if ( process.argv[i] == 'diffs' )
                    command = 'diffs';
                else if ( process.argv[i] == "timestamps")
                    command = "timestamps";
                else if ( process.argv[i] == "delete")
                    command = "delete";
                else
                    sane = false;
            }
            else if ( process.argv[i] == "--help" ){
                command = "help";
                sane = false;
            }
            else if ( process.argv[i] == "--version" ){
                console.log("easysync 0.8.2");
                command = "version";
            }
            else if ( process.argv[i] == "--default-timestamp" )
                default_timestamp = true;
        }
    }
    if ( command == "list_files" ){
        if ( src_dir.length==0 && dst_dir.length==0 )
            sane = false;
    }
    else if ( command == "oneway" || command=="twoway" ){
        if ( src_dir.length == 0 || dst_dir.length==0 )
            sane = false;
    }
    else if ( command == "diffs" ) {
        if ( src_dir.length==0 || dst_dir.length==0 )
            sane = false;
    }
    else if ( command == "delete" ) {
        if ( dst_dir.length==0 && src_dir.length==0 )
            sane=false;
    }
    if ( force.length>0 ) {
        if ( force != "up" && force != "down" )
            sane = false;
    }
    return sane;
}
/**
 * Main function to read args and process command
 */
async function do_all() {
    // main entry point
    if ( read_args() ){
        let log = [];
        // see what we have to do
        if ( command.length > 0 ){
            if ( command == "list_files" )
                await write_file_list(log);
            else if ( command == "twoway" )
                await two_way_sync(log);
            else if ( command == "oneway" )
                await one_way_sync(log);
            else if ( command == "diffs" )
                compute_diffs(log);
            else if ( command == "delete" )
                delete_file(log);
            else if ( command == "timestamps" ) {
                if ( src_dir.length > 0 )
                    await save_time_stamp(src_dir,log);
                if ( dst_dir.length > 0 )
                    await save_time_stamp(dst_dir,log);
            }
            // else version - do nothing
        }
        // no command: sync the sites in config instead
        else {
            // load sites
            read_config(log);
            for ( let site of sites ) {
                src_dir = site.src;
                dst_dir = site.dst;
                if ( site.type == "oneway" )
                    await one_way_sync(log);
                else if ( site.type == "twoway" ) 
                    await two_way_sync(log);
            }
        }
        if ( log.length > 0 ) {
            for ( let i=0;i<log.length;i++ ) 
                console.log(log[i]);
        }
    }
    else
        console.log(help);
}
// call do_all here because it is async
try {
    do_all();
}
catch(err) {
    console.log(err.message);
}

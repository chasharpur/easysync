#!/bin/bash
PWD=`pwd`
./reset.sh
PASSED=1
# new
easysync -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
ssh desmond@ecdosys.com "LINK=\`readlink tests/oneway/test/images/keilawarra\`;if [[ -d \"tests/oneway/test/images\" ]] && [[ -L \"tests/oneway/test/images/keilawarra\" ]] && [[ \$LINK == \"keilawarra-sydney.png\" ]]; then echo OK; fi" > log
LOG=`cat log`
rm log
if [[ -z "$LOG" ]] || [[ $LOG != "OK" ]]; then
    echo "test failed. files not copied to remote server."
    exit 1
fi
rm -rf ./test/images
rm -f ./test/.sync/stamp.json
easysync -d $PWD/test -s desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
LINK=`readlink "$PWD/test/images/keilawarra"`
if [[ -d "$PWD/test/images" ]] && [[ -L "$PWD/test/images/keilawarra" ]] && [[ "$LINK" == "keilawarra-sydney.png" ]]; then
    PASSED=1
else
    echo "failed new"
    PASSED=0
fi
if [ $PASSED -eq 1 ]; then
    echo "test passed"
fi

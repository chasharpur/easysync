#!/bin/bash
./reset.sh
PWD=`pwd`
# update up
./reset.sh
PASSED=1
easysync -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
# backdate server copy
easysync -c timestamps -d desmond@ecdosys.com:/home/desmond/tests/oneway/test --default-timestamp
ssh desmond@ecdosys.com "touch -d \"01 Jan 2001\" /home/desmond/tests/oneway/test/images/keilawarra-sydney.png"
ssh desmond@ecdosys.com "touch -h -d \"01 Jan 2001\" /home/desmond/tests/oneway/test/images/keilawarra"
easysync -c oneway -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test
DATE=`ssh ecdosys.com "stat -c %y /home/desmond/tests/oneway/test/images/keilawarra | cut -d ' ' -f 1"`
if [ "$DATE" == "2001-01-01" ]; then
    echo "update up failed"
    PASSED=0
fi
# update down
if [ $PASSED -eq 1 ]; then
    ./reset.sh
    # copy local files to server
    easysync -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
    # backdate local copy
    easysync -c timestamps -s $PWD/test --default-timestamp
    touch -d "01 Jan 2001" $PWD/test/images/keilawarra-sydney.png
    touch -h -d "01 Jan 2001" $PWD/test/images/keilawarra
    easysync -c oneway -d $PWD/test -s desmond@ecdosys.com:/home/desmond/tests/oneway/test
    DATE=`stat -c %y $PWD/test/images/keilawarra | cut -d ' ' -f 1`
    if [ "$DATE" == "2001-01-01" ]; then
        echo "update down failed"
        PASSED=0
    fi
fi


if [ $PASSED -eq 1 ]; then
    echo "test passed"
else
    echo "test failed"
fi

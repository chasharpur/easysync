#!/bin/bash
PWD=`pwd`
./reset.sh
PASSED=1
# delete down
easysync -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
easysync -c timestamps -d $PWD/test --default-timestamp
ssh desmond@ecdosys.com "rm -rf tests/oneway/test/images"
easysync -d $PWD/test -s desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
if [ -d $PWD/test/images ]; then
    echo "failed to delete down"
    PASSED=0
fi
# delete down
if [ $PASSED -eq 1 ]; then
    ./reset.sh
    # copy to server
    easysync -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
    # remove our copy
    rm -rf test/images
    # push server back in time
    easysync -c timestamps -d desmond@ecdosys.com:/home/desmond/tests/oneway/test --default-timestamp
    ssh desmond@ecdosys.com "touch -d \"01 Jan 2001\" /home/desmond/tests/oneway/test/images/keilawarra-sydney.png"
    ssh desmond@ecdosys.com "touch -h -d \"01 Jan 2001\" /home/desmond/tests/oneway/test/images/keilawarra"
    easysync -c oneway -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test
    ssh desmond@ecdosys.com "if [ ! -e /home/desmond/tests/oneway/test/keilawarra-sydney.png ]; then echo OK; fi">log
    LOG=`cat log`
    rm log
    if [ "$LOG" != "OK" ]; then
        echo "failed to delete down"
        PASSED=0
    fi
fi
if [ $PASSED -eq 1 ]; then
    echo "test passed"
fi

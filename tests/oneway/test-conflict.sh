#!/bin/bash
./reset.sh
PWD=`pwd`
# update up
./reset.sh
PASSED=1
# copy test files to server
easysync -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test -c oneway
# set mod_date of server timestamp and files
ssh desmond@ecdosys.com "touch /home/desmond/tests/oneway/test/images/keilawarra-sydney.png"
ssh desmond@ecdosys.com "touch -h /home/desmond/tests/oneway/test/images/keilawarra"
# backdate local files and timestamp
touch -d "2 hours ago" $PWD/test/.sync/stamp.json
# rewrite timestamp file
node -e "const fs=require('fs');let res_path='$PWD/test/.sync/stamp.json';let str=fs.readFileSync(res_path,{encoding:'utf8'});let obj=JSON.parse(str);let stats=fs.statSync(res_path);obj.time_stamp=stats.mtime.toUTCString();fs.writeFileSync(res_path,JSON.stringify(obj));"
touch -d "1 hour ago" $PWD/test/images/keilawarra-sydney.png
touch -h -d "1 hour ago" $PWD/test/images/keilawarra
# try to copy to server
easysync -c oneway -s $PWD/test -d desmond@ecdosys.com:/home/desmond/tests/oneway/test

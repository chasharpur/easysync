const spawn = require('await-spawn');
const os = require('os');
const path = require('path');
const fs = require('fs');
async function exec_remote(cmd,address,args,log){
    let res = true;
    try {
        args.unshift(cmd);
        let host = address.host;
        if ( address.hasOwnProperty('username') && address.username.length>0 ){
            if ( address.hasOwnProperty("password") && address.password.length>0 )
                host = address.username+':'+address.password+'@'+address.host;
            else
                host = address.username+'@'+address.host;
        }
        args.unshift(host);
        let bl = await spawn('ssh',args);
        if ( bl.length > 0 ) {
            res = false;
            log.push(bl.toString());
        }
    }
    catch ( err ){
        log.push(err.message);
        res = false;
    }
    return res;
}
function parse_address(address) {
    let spec = {};
    let last_colon = address.lastIndexOf(":");
    spec.path = address.substring(last_colon+1);
    let rest = address.substring(0,last_colon);
    let at_pos = rest.indexOf("@");
    if ( at_pos != -1 ){
        spec.host = rest.substring(at_pos+1);
        rest = rest.substring(0,at_pos);
        let colon_pos = rest.indexOf(":");
        if ( colon_pos == -1 )
            spec.username = rest;
        else {
            spec.username = rest.substring(0,colon_pos);
            spec.password = rest.substring(colon_pos+1);
        }
    }
    else
        spec.host = rest;
    return spec;
}
function path_escape(res_path,double) {
    escape_path = "";
    let seen_backslash = false;
    for ( let c of res_path ){
        if ( c == "'" ){
            if ( !seen_backslash ) {
                if ( double )
                    escape_path += "\\'";
                else
                    escape_path += "\'";
            }
            else
                escape_path += c;
        }
        else if ( c == '"' ){
            if ( !seen_backslash ) {
                if ( double )
                    escape_path += "\\'";
                else
                    escape_path += "\'";
            }
            else
                escape_path += c;
        }
        else
            escape_path += c;
        seen_backslash = c=='\\'&&!seen_backslash;
    }
    return escape_path;
}
async function do_all(){
    try{
        let args = [];
        let log = [];
        let to = path_escape(path.join(os.homedir(),"ecdosis-data/annotations/english/harpur/biodocs/ML/A1528/APR-1857-PRINTED_SUBSCRIPTION_LIST_FOR_'WILD_BEE'/3/annotation.json"),false);
        let from = path_escape("ecdosys.com:/home/desmond/ecdosis-data/annotations/english/harpur/biodocs/ML/A1528/APR-1857-PRINTED_SUBSCRIPTION_LIST_FOR_'WILD_BEE'/3/annotation.json",true);
        console.log(from);
        console.log(to);
        args.push("-p");
        args.push(from);
        args.push(to);
        if ( !fs.existsSync(path.dirname(to)) )
            fs.mkdirSync(path.dirname(to),{recursive:true});
        let bl = await spawn("scp",args);
        if ( bl.length > 0 )
            log.push(bl.toString());
        for ( let l of log ) 
            console.log(l);
    }
    catch (e) {
        console.log(e.message);
        console.log(e.stack.split("\n"));
        console.log(e.stderr.toString());
    }
}
do_all();

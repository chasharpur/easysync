# EasySync
EasySync is a program for synchonising files across platforms including Windows, MacOS and 
Linux *that preserves symbolic links* and allows collaboration between groups sharing sets of 
resources mostly consisting of binary (e.g. images or software) or textual files (e.g. JSON) 
with long lines.

## Motivation
There are many solutions available for synchronisation between local and remote file sets that 
are intended to mirror a user's files in the cloud or in a __backup system__. However, these do 
not usually include any handling for the complications that arise when data is shared among 
several users.

__Source code management__ (SCM) systems are designed to handle the sharing of computer code 
between developers. They are based on diff algorithms that reduce the differences between 
updated versions of the same file to a set of 'deltas', which can be transmitted to the server 
and saved, so avoiding copying the entire file. However, this method does not work well for 
binary files or even for text files consisting of only one line, as in the case of unformatted 
JSON. Also SCM systems copy each file at least once to track changes to it. Since binary files 
are often large, their duplication and versioning results in a significant increase in storage 
requirements. So for a 30GB archive of images shared using SCM, 100GB would typically be 
needed, both on the server and on each user's computer. Since storage costs money, this can 
work out to be quite expensive.

A third option is to use __rsync__. This uses a diff algorithm based on blocks rather than 
lines. But for many types of image, particularly JPEG images, or software, the entire file 
changes after each save. This slows it down when simply downloading the entire file without 
computing differences is probably faster.

A fourth problem is that both SCM and rsync have complex user interfaces even when augmented by a 
GUI. Users must learn to resolve conflicts, or to set numerous settings of an arcane nature.

A fifth problem is that none of the above programs preserve symbolic links across platforms. 
Windows has supported symlinks since Vista. And yet no progam I can find supports symlinks on 
Windows. Git restores them on Unix file systems but simply copies the contents of the symlink file 
on Windows. This is a serious problem for image archives, which may contain numerous symlinks to 
save space.

## Aim of EasySync
The problem EasySync aims to solve therefore is to provide a tool that is easy to use: with a 
single button "Sync" which performs one-way or two-way synchronisation of sets of files without 
duplication or backup. In practice backing up a set of files needs to be performed only once, not 
multiple times by each user. So backup will be supplied by an external tool, not by EasySync.

One-way synchronisation is the copying of updated versions of files from a server-based copy to a 
local computer. This can be used to update a user's local copy of shared software which he/she has 
no intention of ever changing.

Two-way synchronisation copies both the user's updated versions of files to the shared repository 
and also pulls down files newly updated by his/her colleagues.

Conflicts can be resolved by overwriting one copy forcefully by another. This has to be done 
explicitly, however. By default conflicts are left unresolved and the user is merely altered to 
their existence. Collaborators should arrange in advance which parts of the archive they will 
work on to avoid conflicts. If they arise, however, subsequent discussion can be used to decide 
which version should be preferred over the other. Merging is impratcical for images or software 
and even for JSON where syntax must be preserved.

Symlinks are *preserved transparently* without the user taking any special action on any 
supported platform such as Windows and MacOS. On Windows, since modification dates on symlinks 
cannot be updated, a shadow directory structure that mirrors the symlink names and their paths
is used to store their modification dates.

## Installation
The application runs from its own folder, so requires no special installation other than 
downloading the tarball and unpacking it. For correct function another copy of easysync must be 
installed on the remote machine.

The application can be run on the commandline. However, it is envisaged that most users will use 
EasySync via the GUI, which simply calls the commandline tool.

EasySync looks for a file config.json in the folder .easysync in the user's home directory. It 
is *required* if the GUI is used. This should contain a list of sites and folders for syncing. It 
contains one key:

*sites*: an array of optional sites. Each of these is a JSON object containing a src, a dst and 
a type. If they designate local directories src or dst are absolute paths. If they designate 
remote directories the paths may be relative to the user's login home directory. Host names 
follow the scp syntax: host name may optionally be preceded by a username separated by '@', and 
the username itself may optinally be followed by a colon containing a password. The type field 
is either oneway or twoway. If oneway, movement of files is from src to dst. e.g.

    {"sites":[{"dst":"/home/bloggs/ecdosis-data","src":"shareduser@mysite.com:shared-data","type":"twoway"},
    {"dst":"/home/bloggs/ecdosis-software-linux","src":"shareduser@mysite.com:ecdosis-software-linux","type":"oneway"}]}

This defines two folders, one a copy of software that will be updated in one direction only --
from the server to the laptop used by user "bloggs" and another folder shared two-way (user's 
changes go to server and shared changes already on server go to user).

If Easysync does not find the config file it expects source and destination directories to be 
provided on the commandline. If none are provided it does nothing. Since the GUI cannot specify 
src and dst directories it will do nothing without a config file.

## GUI
The GUI easysync-gui requires python3 with tkinter and the Python package PySimpleGUI, and at 
least Python 3.6. Tkinter is not a package and is either compiled into Python or not. 
PySimpleGUI can be installed on the commandline using pip:

    pip3 install PySimpleGUI

On Linux the GUI can be launched from the commandline:
    ./easysync-gui &
or via double-clinking on Linux using the dconf-editor 
(https://itectec.com/ubuntu/ubuntu-how-to-execute-a-script-just-by-double-clicking-like-exe-files-in-windows/).

On MacOS an automator app is provided to launch easysync-gui when double-clicked: 
https://support.apple.com/en-au/guide/automator/autbbd4cc11c/mac

A Windows tool for launching the gui is not yet provided.

The easysync-gui file takes one commandline argument 'ecdosis', which if provided adds controls for 
Ecdosis. The default is to only display controls for Easysync.

## Resolving conflicts
Conflicts arise when a file is updated by someone else and also by you since your last download. 

By default EasySync does not resolve conflicts. The GUI has a set of three radio buttons: up, 
down and none (don't). If *none* is selected then conflicts are printed to the console but not 
updated. If *up* is selected the local version is used to overwrite the server version. If 
*down* is selected the server version is used to overwrite the local version.

## Setup for sharing files
To set up easysync for sharing between more than two users you *must* create a shared user on 
the server and make that user the owner of all the files to be shared. So if your shared folder 
was mydata and your shared user was shareduser you would issue the command

    chown -R shareduser shared-data

Easysync does not, unlike rsync, use a daemon run by root, so in order to update timestamps the 
local user must possess a private key of shareduser. Ssh on the server must then be configured 
to only accept logins from shareduser via this private key. Alternatively add the username and 
password into the config file, e.g.

    ..."src":"shareduser:secret@mysite.com:shared-data" ...

But that is significantly less secure than passwordless login. There are plenty of guides on 
the Web explaining how to set up passwordless login for ssh.

## Rebuilding
EasySync consists of a single node.js javascript file: sync.js. To regenerate the binary run 
the rebuild-<platform>.sh scripts. These copy the binary to ecdosis-software-<platform>, where 
'platform; is macos or linux.

